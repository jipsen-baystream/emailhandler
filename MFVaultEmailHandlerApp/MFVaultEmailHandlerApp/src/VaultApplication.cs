﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;

namespace MFVaultEmailHandlerApp
{
    /// <summary>
    /// Simple configuration.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Reference to a test class.
        /// </summary>
        [MFClass(Required = false)]
        public MFIdentifier TestClassID = "FailAlias";
    }

    /// <summary>
    /// Simple vault application to demonstrate VAF.
    /// </summary>
    public class VaultApplication : VaultApplicationBase
    {
        /// <summary>
        /// Simple configuration member. MFConfiguration-attribute will cause this member to be loaded from the named value storage from the given namespace and key.
        /// Here we override the default alias of the Configuration class with a default of the config member.
        /// The default will be written in the named value storage, if the value is missing.
        /// Use Named Value Manager to change the configurations in the named value storage.
        /// </summary>
        [MFConfiguration("MFVaultApplication1", "config")]
        private Configuration config = new Configuration() { TestClassID = "TestClassAlias" };

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailFrom_PropertyDef = "B.PD.Email_From";

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailFrom_Contact = "B.PD.Email_From_Contact";

        [MFPropertyDef(Required = true)]
        MFIdentifier Contact_EmailId_PropertyDef = "B.PD.Email_Address";

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailTo_PropertyDef = "B.PD.Email_To";

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailTo_Contacts = "B.PD.Email_To_Contacts";

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailCC_PropertyDef = "B.PD.Email_CC";

        [MFPropertyDef(Required = true)]
        MFIdentifier EmailCC_Contacts = "B.PD.Email_CC_Contacts";

        [MFObjType(Required = true)]
        MFIdentifier Contact_ObjectType = "B.OT.Contact";

        [MFClass(Required = true)]
        MFIdentifier EmailMessageObjectType = "B.DC.Email_Message_Document";

        bool IsEmailFromSet = false;
        bool IsEmalCCSet = false;
        bool IsEmailToSet = false;

        /// <summary>
        /// The method, that is run when the vault goes online.
        /// </summary>
        protected override void StartApplication()
        {
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.DC.Email_Message_Document")]
        //  [EventHandler(MFEventHandlerType.MFEventHandlerAfterSetProperties, Class = "B.DC.Email_Message_Document")]
        public void onNewEmailCreated(EventHandlerEnvironment env)
        {
            try
            {
                if (env.EventType == MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize)
                {
                    ContactUpdate_when_object_creation(env);
                }
                else
                {
                    // ContactUpdate(env);
                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);

                IsEmailFromSet = false;
                IsEmailToSet = false;
                IsEmalCCSet = false;
            }
        }

        private void ContactUpdate_when_object_creation(EventHandlerEnvironment env)
        {
            SysUtils.ReportToEventLog("Object Creation Started", System.Diagnostics.EventLogEntryType.Information);

            PropertyValue propertyValue = new PropertyValue();
            Vault vault = env.Vault;
            ObjVer objVer = env.ObjVer;
            PropertyValues propertyValues = env.PropertyValues;
            dynamic result;
            SearchConditions conditions = new SearchConditions();
            SearchCondition searchCondtion = new SearchCondition();
            int objId;
            List<int> contactLists = new List<int>();
            propertyValue = propertyValues.SearchForProperty(EmailFrom_PropertyDef);

            if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
            {
                SysUtils.ReportToEventLog("Property value is null :" + EmailFrom_PropertyDef, System.Diagnostics.EventLogEntryType.Information);
            }
            else
            {
                SysUtils.ReportToEventLog("Email From :" + propertyValue.Value.DisplayValue, System.Diagnostics.EventLogEntryType.Information);

                searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(propertyValue.Value.DisplayValue));
                searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                conditions.Add(-1, searchCondtion);

                searchCondtion = new SearchCondition();
                searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                conditions.Add(0, searchCondtion);

                result = vault.ObjectSearchOperations
                    .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                     .Cast<ObjectVersion>()
                     .FirstOrDefault();

                if (result != null)
                {
                    objId = result.ObjVer.ID;
                    vault.ObjectPropertyOperations.SetProperty(objVer, GetEmailFromContactPropertyValue(objId));
                    SysUtils.ReportToEventLog("Email From is set", System.Diagnostics.EventLogEntryType.Information);
                }
                else
                {
                    SysUtils.ReportToEventLog("From Email Contact Not found :" + propertyValue.Value.DisplayValue, System.Diagnostics.EventLogEntryType.Information);
                }
            }

            propertyValue = propertyValues.SearchForProperty(EmailCC_PropertyDef);

            if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
            {
                SysUtils.ReportToEventLog("Property value is null for the definitin :" + EmailCC_PropertyDef, System.Diagnostics.EventLogEntryType.Information);
            }
            else
            {
                string[] emailCCs = propertyValue.Value.Value.ToString().Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                contactLists = new List<int>();
                foreach (string emailCC in emailCCs)
                {
                    conditions = new SearchConditions();
                    searchCondtion = new SearchCondition();

                    SysUtils.ReportToEventLog("Email CC :" + emailCC, System.Diagnostics.EventLogEntryType.Information);

                    searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(emailCC.Trim()));
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                    conditions.Add(-1, searchCondtion);

                    searchCondtion = new SearchCondition();
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                    searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                    conditions.Add(0, searchCondtion);

                    result = vault.ObjectSearchOperations
                    .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                     .Cast<ObjectVersion>()
                     .FirstOrDefault();

                    if (result != null)
                    {
                        objId = result.ObjVer.ID;
                        if (!contactLists.Contains(objId))
                            contactLists.Add(objId);
                    }
                    else
                    {
                        SysUtils.ReportToEventLog("Contact not found for Email CC:" + emailCC, System.Diagnostics.EventLogEntryType.Information);
                    }
                }

                PropertyValue cc = GetEmailCCContactPropertyValue(EmailCC_Contacts, contactLists.ToArray());

                if (cc == null)
                {
                    SysUtils.ReportToEventLog("Returns null Email GetEmailCCContactPropertyValue:" + EmailCC_Contacts, System.Diagnostics.EventLogEntryType.Information);
                }
                else
                    vault.ObjectPropertyOperations.SetProperty(objVer, cc);

                SysUtils.ReportToEventLog("Email CC is set", System.Diagnostics.EventLogEntryType.Information);

            }

            propertyValue = propertyValues.SearchForProperty(EmailTo_PropertyDef);

            if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
            {
                SysUtils.ReportToEventLog("propertyValue is null for the definition:" + EmailTo_PropertyDef, System.Diagnostics.EventLogEntryType.Information);
            }
            else
            {
                string[] emailTos = propertyValue.Value.Value.ToString().Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                contactLists = new List<int>();
                foreach (string emailTo in emailTos)
                {
                    conditions = new SearchConditions();
                    searchCondtion = new SearchCondition();

                    SysUtils.ReportToEventLog("Email To:" + emailTo, System.Diagnostics.EventLogEntryType.Information);


                    searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(emailTo.Trim()));
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                    conditions.Add(-1, searchCondtion);

                    searchCondtion = new SearchCondition();
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                    searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                    conditions.Add(0, searchCondtion);

                    result = vault.ObjectSearchOperations
                    .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                     .Cast<ObjectVersion>()
                     .FirstOrDefault();

                    if (result != null)
                    {
                        objId = result.ObjVer.ID;
                        if (!contactLists.Contains(objId))
                            contactLists.Add(objId);
                    }
                    else
                    {
                        SysUtils.ReportToEventLog("Email to contact not found for :" + emailTo, System.Diagnostics.EventLogEntryType.Information);
                    }
                }

                vault.ObjectPropertyOperations.SetProperty(objVer, GetEmailCCContactPropertyValue(EmailTo_Contacts, contactLists.ToArray()));
                SysUtils.ReportToEventLog("Email To is set", System.Diagnostics.EventLogEntryType.Information);
            }

            SysUtils.ReportToEventLog("Object Creation is complete", System.Diagnostics.EventLogEntryType.Information);

        }

        private void ContactUpdate(EventHandlerEnvironment env)
        {

            SysUtils.ReportToEventLog("Contact update is running", System.Diagnostics.EventLogEntryType.Information);

            PropertyValue propertyValue = new PropertyValue();
            Vault vault = env.Vault;
            ObjVer objVer = env.ObjVer;
            PropertyValues propertyValues = env.PropertyValues;
            dynamic result;
            SearchConditions conditions = new SearchConditions();
            SearchCondition searchCondtion = new SearchCondition();
            int objId;
            List<int> contactLists = new List<int>();

            if (!IsEmailFromSet)
            {
                IsEmailFromSet = true;
                propertyValue = propertyValues.SearchForProperty(EmailFrom_PropertyDef);

                if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
                {
                    SysUtils.ReportToEventLog("propertyValue is null for the defintion : " + EmailFrom_PropertyDef, System.Diagnostics.EventLogEntryType.Information);
                }
                else
                {
                    searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(propertyValue.Value.DisplayValue));
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                    conditions.Add(-1, searchCondtion);

                    searchCondtion = new SearchCondition();
                    searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                    searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                    searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                    conditions.Add(0, searchCondtion);

                    result = vault.ObjectSearchOperations
                        .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                         .Cast<ObjectVersion>()
                         .FirstOrDefault();

                    if (result == null)
                    {
                        SysUtils.ReportToEventLog("Email From contact not found : " + propertyValue.Value.DisplayValue, System.Diagnostics.EventLogEntryType.Information);
                    }
                    else
                    {
                        objId = result.ObjVer.ID;
                        vault.ObjectPropertyOperations.SetProperty(objVer, GetEmailFromContactPropertyValue(objId));
                    }
                }
            }

            if (!IsEmalCCSet)
            {
                IsEmalCCSet = true;
                propertyValue = propertyValues.SearchForProperty(EmailCC_PropertyDef);

                if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
                {
                    SysUtils.ReportToEventLog("property value is null for the defintionn :" + propertyValue, System.Diagnostics.EventLogEntryType.Information);
                }
                else
                {
                    string[] emailCCs = propertyValue.Value.Value.ToString().Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    contactLists = new List<int>();
                    foreach (string emailCC in emailCCs)
                    {
                        conditions = new SearchConditions();
                        searchCondtion = new SearchCondition();

                        SysUtils.ReportToEventLog("Email CC :" + emailCC, System.Diagnostics.EventLogEntryType.Information);


                        searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                        searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(emailCC.Trim()));
                        searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                        conditions.Add(-1, searchCondtion);

                        searchCondtion = new SearchCondition();
                        searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                        searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                        searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                        conditions.Add(0, searchCondtion);

                        result = vault.ObjectSearchOperations
                        .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                         .Cast<ObjectVersion>()
                         .FirstOrDefault();

                        if (result != null)
                        {
                            objId = result.ObjVer.ID;
                            contactLists.Add(objId);
                        }
                        else
                        {
                            SysUtils.ReportToEventLog("Email CC contact not found :" + emailCC, System.Diagnostics.EventLogEntryType.Information);
                        }
                    }

                    vault.ObjectPropertyOperations.SetProperty(objVer, GetEmailCCContactPropertyValue(EmailCC_Contacts, contactLists.ToArray()));
                    SysUtils.ReportToEventLog("Email CC is set ", System.Diagnostics.EventLogEntryType.Information);

                }
            }

            if (!IsEmailToSet)
            {
                IsEmailToSet = true;
                propertyValue = propertyValues.SearchForProperty(EmailTo_PropertyDef);

                if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
                {
                    SysUtils.ReportToEventLog("property value is null ", System.Diagnostics.EventLogEntryType.Information);
                }
                else
                {
                    string[] emailTos = propertyValue.Value.Value.ToString().Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    contactLists = new List<int>();
                    foreach (string emailTo in emailTos)
                    {
                        conditions = new SearchConditions();
                        searchCondtion = new SearchCondition();

                        SysUtils.ReportToEventLog("Email To:" + emailTo, System.Diagnostics.EventLogEntryType.Information);


                        searchCondtion.Expression.SetPropertyValueExpression(Contact_EmailId_PropertyDef, MFParentChildBehavior.MFParentChildBehaviorNone);
                        searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeText, GetEmail(emailTo.Trim()));
                        searchCondtion.ConditionType = MFConditionType.MFConditionTypeContains;

                        conditions.Add(-1, searchCondtion);

                        searchCondtion = new SearchCondition();
                        searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                        searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                        searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)Contact_ObjectType);

                        conditions.Add(0, searchCondtion);

                        result = vault.ObjectSearchOperations
                        .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                         .Cast<ObjectVersion>()
                         .FirstOrDefault();

                        if (result != null)
                        {
                            objId = result.ObjVer.ID;
                            contactLists.Add(objId);
                        }
                        else
                        {
                            SysUtils.ReportToEventLog("Contact not found fr Email To:" + emailTo, System.Diagnostics.EventLogEntryType.Information);
                        }
                    }

                    vault.ObjectPropertyOperations.SetProperty(objVer, GetEmailCCContactPropertyValue(EmailTo_Contacts, contactLists.ToArray()));
                    SysUtils.ReportToEventLog("Email To is set", System.Diagnostics.EventLogEntryType.Information);
                }
            }
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCheckOut, Class = "B.DC.Email_Message_Document")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.DC.Email_Message_Document")]
        public void onCheckOutOrCheckIn(EventHandlerEnvironment env)
        {
            //PropertyValue propertyValue;
            //PropertyValues propertyValues = env.PropertyValues;
            //if(propertyValues ==null)
            //{
            //    propertyValues = env.Vault.ObjectPropertyOperations.GetProperties(env.ObjVer);
            //}
            //propertyValue = propertyValues.SearchForProperty(EmailFrom_Contact);

            //if (propertyValue == null || string.IsNullOrEmpty(propertyValue.Value?.Value?.ToString()))
            //{
            IsEmailToSet = false;

            IsEmalCCSet = false;
            IsEmailFromSet = false;

            //  ContactUpdate(env);
            // }
        }

        private PropertyValue GetEmailCCContactPropertyValue(MFIdentifier identifier, int[] contactObjs)
        {
            try
            {
                PropertyValue propertyValue = new PropertyValue();
                propertyValue.PropertyDef = identifier;
                propertyValue.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, contactObjs);
                return propertyValue;
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error occured on GetEmailCCContactPropertyValue: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

                throw ex;
            }
        }

        private PropertyValue GetEmailFromContactPropertyValue(int conactObjId)
        {
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.PropertyDef = EmailFrom_Contact;
            propertyValue.TypedValue.SetValue(MFDataType.MFDatatypeLookup, conactObjId);
            return propertyValue;
        }

        private string GetEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return string.Empty;
            }

            string emailId = string.Empty;

            int startIndex;
            int length;

            if (email.IndexOf("<") > 0 && email.IndexOf(">") > 0)
            {
                startIndex = email.IndexOf("<") + 1;
                length = email.IndexOf(">") - startIndex;
                emailId = email.Substring(startIndex, length);
                return emailId;
            }

            return email;
        }
    }
}